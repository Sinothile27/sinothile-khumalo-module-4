import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:smg_app/screens/home_screen.dart';
import 'package:smg_app/screens/signin_screen.dart';
import 'package:smg_app/screens/signup_screen.dart';
import 'package:smg_app/screens/splash_screen.dart';

class DirectoryScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DirectoryScreenState();
  }
}

class _DirectoryScreenState extends State<DirectoryScreen> {
  double _drawerIconSize = 24;
  double _drawerFontSize = 17;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => MakeDashboardItems())),
        ),
        title: Text('MY DIRECTORY'),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.fromLTRB(25, 10, 25, 10),
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      border: Border.all(width: 5, color: Colors.white),
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding:
                              const EdgeInsets.only(left: 8.0, bottom: 4.0),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Contact List",
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Card(
                          child: Container(
                            alignment: Alignment.topLeft,
                            padding: EdgeInsets.all(15),
                            child: Column(
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    ...ListTile.divideTiles(
                                      color: Colors.grey,
                                      tiles: [
                                        ListTile(
                                          leading: Icon(Icons.phone),
                                          title: Text("User 1"),
                                          subtitle: Text("00-123-456-78"),
                                        ),
                                        ListTile(
                                          leading: Icon(Icons.phone),
                                          title: Text("User 2"),
                                          subtitle: Text("01-234-567-89"),
                                        ),
                                        ListTile(
                                          leading: Icon(Icons.phone),
                                          title: Text("User 3"),
                                          subtitle: Text("02-345-678-90"),
                                        ),
                                        ListTile(
                                          leading: Icon(Icons.phone),
                                          title: Text("User 4"),
                                          subtitle: Text("02-345-678-90"),
                                        ),
                                        ListTile(
                                          leading: Icon(Icons.phone),
                                          title: Text("User 5"),
                                          subtitle: Text("02-345-678-90"),
                                        )
                                      ],
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
