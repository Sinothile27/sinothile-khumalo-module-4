import 'package:flutter/material.dart';
import 'package:smg_app/screens/home_screen.dart';
import 'package:smg_app/screens/signin_screen.dart';
import 'package:smg_app/screens/signup_screen.dart';
import 'package:smg_app/screens/splash_screen.dart';
import 'package:smg_app/views/profile_screen.dart';

// Create a Flutter app that has the following screens,
//navigating to each other. Your app must have up to 6 screens
//(login, dashboard, 2 feature screens, and user profile edit):

// Login and registration screens (not linked to a database) with relevant input fields.
// Dashboard (the screen after login) with buttons to feature screens of your app.
// The last screen must be of a user profile edit.
// Dashboard Floating button on the home screen, linking to another screen.
// Each screen must be labeled appropriately on the app bar.

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(
        title: '',
      ),
    );
  }
}
